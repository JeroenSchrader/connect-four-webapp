export class Board {
  public width: number;
  public height: number;
  public grid: boolean[][];

  constructor(width: number, height: number, grid: boolean[][]) {
    this.width = width;
    this.height = height;
    this.grid = grid;
  }
}
