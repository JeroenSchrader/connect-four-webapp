import { Player } from "./Player";
import { Board } from "./Board";

export class MultiplayerGame {
  public gameId: string;
  public players: Player[];
  public inProgress: boolean;
  public finished: boolean;
  public winner: Player;
  public currentPlayerTurn: Player;
  public board: Board;

  constructor(
    gameId: string,
    players: Player[],
    inProgress: boolean,
    finished: boolean,
    winner: Player,
    currentPlayerTurn: Player,
    board: Board
  ) {
    this.gameId = gameId;
    this.players = players;
    this.inProgress = inProgress;
    this.finished = finished;
    this.winner = winner;
    this.currentPlayerTurn = currentPlayerTurn;
    this.board = board;
  }
}
