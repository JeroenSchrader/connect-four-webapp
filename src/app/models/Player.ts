export class Player {
  public connectionId: string;
  public idInGame: boolean;
  public name: string;

  constructor(connectionId: string, idInGame: boolean, name: string) {
    this.connectionId = connectionId;
    this.idInGame = idInGame;
    this.name = name;
  }
}
