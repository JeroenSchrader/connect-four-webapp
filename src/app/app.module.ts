import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./components/home/home.component";
import { SingleplayergameComponent } from "./components/singleplayergame/singleplayergame.component";
import { MultiplayergameComponent } from "./components/multiplayergame/multiplayergame.component";
import { GameboardComponent } from "./components/gameboard/gameboard.component";
import { ChipComponent } from "./components/chip/chip.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SingleplayergameComponent,
    MultiplayergameComponent,
    GameboardComponent,
    ChipComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
