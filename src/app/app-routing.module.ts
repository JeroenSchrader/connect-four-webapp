import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "../app/components/home/home.component";
import { SingleplayergameComponent } from "../app/components/singleplayergame/singleplayergame.component";
import { MultiplayergameComponent } from "../app/components/multiplayergame/multiplayergame.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "singleplayer", component: SingleplayergameComponent },
  { path: "multiplayer", component: MultiplayergameComponent },
  { path: "**", component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
