import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { SingleplayerGame } from "../models/Singleplayergame";

@Injectable({
  providedIn: "root",
})
export class SinglePlayerService {
  private url: string = environment.singleplayerUrl;
  private gameId: string;
  private player1Name: string;
  private player2Name: string;
  public gameInfo: SingleplayerGame;

  constructor(private http: HttpClient) {}

  playersReady(): boolean {
    return (
      this.player1Name &&
      this.player1Name !== null &&
      this.player2Name &&
      this.player2Name !== null
    );
  }

  initializeNewGame(): any {
    if (
      this.player1Name &&
      this.player1Name !== null &&
      this.player2Name &&
      this.player2Name !== null
    ) {
      this.http
        .get<SingleplayerGame>(
          `${this.url}/newgame/${this.player1Name}/${this.player2Name}`
        )
        .pipe(catchError((error) => this._handleError(error)))
        .subscribe((response) => {
          this.gameId = response.gameId;
          this.gameInfo = response;
        });
    }
  }

  addChip(columnIndex: number): any {
    if (this.gameId && this.gameId !== null && this.gameId !== "") {
      this.http
        .get<SingleplayerGame>(`${this.url}/${this.gameId}/${columnIndex}`)
        .pipe(catchError((error) => this._handleError(error)))
        .subscribe((response) => (this.gameInfo = response));
    }
  }

  setPlayerNames(player1Name: string, player2Name: string) {
    this.player1Name = player1Name;
    this.player2Name = player2Name;
  }

  private _handleError(err: HttpErrorResponse | any): Observable<any> {
    const errMessage = err.message || "Something went wrong";
    return throwError(errMessage);
  }
}
