import { HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { environment } from "src/environments/environment";
import * as signalR from "@aspnet/signalr";
import { MultiplayerGame } from "../models/MultiplayerGame";

export class Received {
  static newBoardState: string = "sendNewBoardState";
  static playerDisconnected: string = "sendPlayerDisconnected";
}

export class ClientOptions {
  static addChip: string = "addChip";
  static setName: string = "setName";
}

@Injectable({
  providedIn: "root",
})
export class MultiplayerService {
  private url: string = environment.multiplayerUrl;
  private hubConnection: signalR.HubConnection;

  public initializeConnection(): any {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(this.url)
      .build();

    this.hubConnection
      .start()
      .then(() => {
        console.log("Connected to multiplayer service");
        this.hubConnection.send(ClientOptions.setName, this.playerName);
      })
      .catch((err) => this._handleError(err));

    this.lookingForGame = true;

    this.hubConnection.on(
      Received.newBoardState,
      (gameInfo: MultiplayerGame) => {
        this.gameInfo = gameInfo;
        this.gameInProgress = gameInfo.inProgress;
        this.lookingForGame = false;
      }
    );

    this.hubConnection.on(Received.playerDisconnected, () => {
      this.otherPlayerDisconnected = true;
      this.gameInProgress = false;
    });

    this.hubConnection.onclose((err) => {
      this.lookingForGame = false;
      this.gameInProgress = false;
      this._handleError(err);
    });
  }

  private lookingForGame: boolean;
  private playerName: string;
  public otherPlayerDisconnected: boolean;
  public gameInfo: MultiplayerGame;
  public gameInProgress: boolean;

  constructor() {}

  playerReady(): boolean {
    return this.playerName && this.playerName !== null;
  }

  addChip(columnIndex: number): any {
    if (this.gameInfo && this.gameInProgress && !this.otherPlayerDisconnected) {
      this.hubConnection
        .invoke(ClientOptions.addChip, this.playerName, columnIndex)
        .catch((err) => this._handleError(err));
    }
  }

  setPlayerName(playerName: string) {
    this.playerName = playerName;
  }

  reset() {
    if (this.hubConnection) {
      this.hubConnection.stop();
    }
    this.hubConnection = null;
    this.playerName = "";
    this.otherPlayerDisconnected = false;
    this.lookingForGame = false;
    this.gameInfo = null;
    this.gameInProgress = false;
  }

  private _handleError(err: HttpErrorResponse | any): Observable<any> {
    const errMessage = (err && err.message) || "Something went wrong";
    return throwError(errMessage);
  }
}
