import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { Board } from "src/app/models/Board";

@Component({
  selector: "app-gameboard",
  templateUrl: "./gameboard.component.html",
  styleUrls: ["./gameboard.component.scss"],
})
export class GameboardComponent implements OnInit {
  @Input() boardInfo: Board;
  @Output() columnClicked = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {}

  onClick(columnIndex: number) {
    this.columnClicked.emit(columnIndex);
  }
}
