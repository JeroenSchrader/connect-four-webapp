import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SingleplayergameComponent } from "./singleplayergame.component";

describe("SingleplayergameComponent", () => {
  let component: SingleplayergameComponent;
  let fixture: ComponentFixture<SingleplayergameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SingleplayergameComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleplayergameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
