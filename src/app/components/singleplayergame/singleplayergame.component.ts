import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SinglePlayerService } from "src/app/services/singleplayer.service";

@Component({
  selector: "app-singleplayergame",
  templateUrl: "./singleplayergame.component.html",
  styleUrls: ["./singleplayergame.component.scss"],
})
export class SingleplayergameComponent implements OnInit {
  constructor(
    private singlePlayerService: SinglePlayerService,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.singlePlayerService.playersReady()) {
      this.newGame();
    } else {
      this.router.navigateByUrl("");
    }
  }

  newGame() {
    this.singlePlayerService.initializeNewGame();
  }

  makeMove(columnIndex: number) {
    this.singlePlayerService.addChip(columnIndex);
  }
}
