import { Component, Input, OnInit, Output } from "@angular/core";
import { EventEmitter } from "protractor";

@Component({
  selector: "app-chip",
  templateUrl: "./chip.component.html",
  styleUrls: ["./chip.component.scss"],
})
export class ChipComponent implements OnInit {
  @Input() value: boolean;

  constructor() {}

  ngOnInit() {}
}
