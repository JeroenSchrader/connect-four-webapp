import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MultiplayerService } from "src/app/services/multiplayer.service";

@Component({
  selector: "app-multiplayergame",
  templateUrl: "./multiplayergame.component.html",
  styleUrls: ["./multiplayergame.component.scss"],
})
export class MultiplayergameComponent implements OnInit {
  constructor(
    private multiplayerService: MultiplayerService,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.multiplayerService.playerReady()) {
      this.multiplayerService.initializeConnection();
    } else {
      this.router.navigateByUrl("");
    }
  }

  makeMove(columnIndex: number) {
    this.multiplayerService.addChip(columnIndex);
  }
}
