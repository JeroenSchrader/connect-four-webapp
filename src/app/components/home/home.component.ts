import { Component, OnInit } from "@angular/core";
import { SinglePlayerService } from "src/app/services/singleplayer.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { MultiplayerService } from "src/app/services/multiplayer.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  private menuForm: FormGroup;

  constructor(
    private singlePlayerService: SinglePlayerService,
    private multiplayerService: MultiplayerService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.menuForm = this.formBuilder.group({
      player1Name: ["", Validators.required],
      player2Name: [""],
      multiplayerToggle: [false],
    });
  }

  ngOnInit() {
    this.multiplayerService.reset();
  }

  onSubmit(formData) {
    if (this.menuForm.valid) {
      if (formData.multiplayerToggle) {
        ///Initialize multiplayer game
        this.multiplayerService.setPlayerName(formData.player1Name);
        this.router.navigateByUrl("/multiplayer");
      } else {
        ///Initialize singleplayer game
        this.singlePlayerService.setPlayerNames(
          formData.player1Name,
          formData.player2Name
        );
        this.router.navigateByUrl("/singleplayer");
      }
    }
  }
}
